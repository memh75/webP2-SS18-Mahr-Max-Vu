import {Request, Response} from "express";
import express = require("express");
import bodyParser = require("body-parser");

let router = express();
router.use(bodyParser.json());
router.listen(8080, () => {
  console.log("Gestartet!");
  console.log("Aufrufbar sollten sein: ");
  console.log("  http://localhost:8080/name");
  console.log("  http://localhost:8080/htmlfile");
  console.log("  http://localhost:8080/gauss");
  console.log("  http://localhost:8080/liste");
  console.log("  http://localhost:8080/object");
});
router.get("/name", function(req: Request, res: Response){
    res.json({vorname: "Max", nachname: "Mahr"});
});

router.use("/htmlfile", express.static(__dirname + '/name.html'));

router.get("/gauss", function (req:Request, res: Response){
  let a: number = 0;
  for (let i=1; i>=100; i++){
      a += i
}

res.json({ergebnis: a});
});

router.get("/liste", function (req: Request, res: Response) {
    let Zahlen: number[] = [];

    for (let i = 1; i <= 100; i++) {
        Zahlen.push(i)
    }
    res.json({ergebnis: Zahlen});
});

class Person {
    public vorname: string;
    public nachname: string;

    constructor(_vorname: string, _nachname: string) {
        this.vorname = _vorname;
        this.nachname = _nachname;
    }
}

router.get("/object", function (req: Request, res: Response) {
    let myObject = new Person("Max", "Mahr");
    res.json(myObject);
});



