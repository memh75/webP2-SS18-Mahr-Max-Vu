function getSession() {
    $.ajax({
        url: 'http://localhost:8080/session',
        type: 'GET',
        dataType: 'json',
        success: function (data) {
            console.log("getSession", data);
            $('#SessionCode').find('code').html(JSON.stringify(data));
        },
        error: function (jqXHR) {
            $('#demo').attr({ msg: jqXHR.responseJSON.message });
            $('#demo').click();
        }
    });
}
function deleteSession() {
    $.ajax({
        url: 'http://localhost:8080/session',
        type: 'DELETE',
        dataType: 'json',
        success: function (data) {
            console.log("deleteSession", data);
            getSession();
            $('#demo').attr({ msg: "Session gelöscht!" });
            $('#demo').click();
        },
        error: function (jqXHR) {
            $('#demo').attr({ msg: jqXHR.responseJSON.message });
            $('#demo').click();
        }
    });
}
function Variablesetzen() {
    var key = $('#deleteVariableKey');
    var val = $('#Variableanlegen');
    if (key.val() != "" && val.val() != "") {
        $.ajax({
            url: 'http://localhost:8080/variable/' + key.val() + '/' + val.val(),
            type: 'POST',
            dataType: 'json',
            success: function (data) {
                console.log("setVariable", data);
                getSession();
                key.val("").parent().removeClass('is-dirty');
                val.val("").parent().removeClass('is-dirty');
                $('#demo').attr({ msg: "Session-Variable gesetzt!" });
                $('#demo').click();
            },
            error: function (jqXHR) {
                $('#demo').attr({ msg: jqXHR.responseJSON.message });
                $('#demo').click();
            }
        });
    }
}
function Variablelöschen() {
    var key = $('#deleteVariableKey');
    if (key.val() != "") {
        $.ajax({
            url: 'http://localhost:8080/variable/' + key.val(),
            type: 'DELETE',
            dataType: 'json',
            success: function (data) {
                console.log("deleteVariable", data);
                getSession();
                key.val("").parent().removeClass('is-dirty');
                $('#demo').attr({ msg: "Session-Variable gelöscht!" });
                $('#demo').click();
            },
            error: function (jqXHR) {
                $('#demo').attr({ msg: jqXHR.responseJSON.message });
                $('#demo').click();
            }
        });
    }
}
$(document).ready(function () {
    getSession();
    $("#getSessionBtn").on("click", function () {
        getSession();
    });
    $("#deleteSessionBtn").on("click", function () {
        deleteSession();
    });
    $("#VariableBtn").on("click", function () {
        Variablesetzen();
    });
    $("#deleteVariableBtn").on("click", function () {
        Variablelöschen();
    });
});
//# sourceMappingURL=script.js.map