var AJAX;
(function (AJAX) {
    /**********************************************************************************************************************
     * Class "User": represents all data of a user in the userList - class is exported  (needs ES6)                       *
     **********************************************************************************************************************/
    var User = /** @class */ (function () {
        function User() {
        }
        return User;
    }());
    /*****************************************************************************/
    /* render function: takes a message and a userData-array                     */
    /*****************************************************************************/
    function render(message, userList) {
        var buffer = "";
        //--- render message --------------------------------------------------------
        buffer += "<div id='serverMessage'> " + message + "</div>\n";
        //--- render table (only if userData exists) --------------------------------
        if (userList.length > 0) {
            buffer += "<div id='userList'>\n";
            buffer += "  <table>\n";
            buffer += "    <tr>\n";
            buffer += "      <th style='width:20px '> id       </th>\n";
            buffer += "      <th style='width:100px'> vorname  </th>\n";
            buffer += "      <th style='width:100px'> nachname </th>\n";
            buffer += "      <th style='width:100px'> nutzername </th>\n";
            buffer += "      <th style='width:100px'> passwort </th>\n";
            buffer += "    </tr>\n";
            for (var _i = 0, userList_1 = userList; _i < userList_1.length; _i++) {
                var user = userList_1[_i];
                if (user != null) { // ignore array-elements that have been deleted
                    buffer += "    <tr id=user'" + user.id + "'>\n";
                    buffer += "      <td> " + user.id + " </td>\n";
                    buffer += "      <td> " + user.vorname + " </td>\n";
                    buffer += "      <td> " + user.nachname + " </td>\n";
                    buffer += "      <td> " + user.nutzername + " </td>\n";
                    buffer += "      <td> " + user.passwort + " </td>\n";
                    buffer += "    </tr>\n";
                }
            }
        }
        //--- close table (and div) ------------------------------------------------
        buffer += "  </table>\n";
        buffer += "</div>";
        //--- put buffer-string into message and userlist --------------------------
        $('#data').html(buffer);
        //
        // Examples of loops: All loops provide the same results
        //
        console.log("\n---------------------\n for-of Schleife)");
        for (var _a = 0, userList_2 = userList; _a < userList_2.length; _a++) {
            var entry = userList_2[_a];
            if (entry != null) { // ignore array-elements that have been deleted
                console.log(entry.vorname + " " + entry.nachname);
            }
        }
        console.log("\n for-in Schleife");
        for (var j in userList) { // j get index of all elements in list
            if (userList[j] != null) {
                console.log(userList[j].vorname + " " + userList[j].nachname);
            }
        }
        console.log("\n for-Schleife)");
        for (var i_1 = 0; i_1 < userList.length; i_1++) { // index starts with 0 and end with length of list-1
            if (userList[i_1] != null) {
                console.log(userList[i_1].vorname + " " + userList[i_1].nachname);
            }
        }
        console.log("\n while-Schleife)");
        var i = 0; // i start with 0
        while (i < userList.length) { // loop runs while i < length of list
            if (userList[i] != null) {
                console.log(userList[i].vorname + " " + userList[i].nachname);
            }
            i++; // increment i - i.e. i=i+1
        }
    }
    /*****************************************************************************/
    /*  "Main" Callback funtion: triggered when page loaded                      */
    /*****************************************************************************/
    $(function () {
        //--- click on the create button --------------------------------------------
        $('#createBtn').on("click", function () {
            var vorname = $('#forenameInput').val().trim();
            var nachname = $('#surnameInput').val().trim();
            var nutzername = $('#nutzernameInput').val().trim();
            var passwort = $('#passwortInput').val().trim();
            var data = { "vorname": vorname, "nachname": nachname, "nutzername": nutzername, "passwort": passwort };
            $.ajax({
                url: 'http://localhost:8080/user',
                type: 'POST',
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) { render(data.message, data.userList); },
                error: function (jqXHR) { render(jqXHR.responseJSON.message, []); }
            });
        });
        //--- click on the read button ----------------------------------------------
        $('#readBtn').on("click", function () {
            var id = $('#userIDInput').val();
            $.ajax({
                url: 'http://localhost:8080/user/' + id,
                type: 'GET',
                dataType: 'json',
                success: function (data) { render(data.message, data.userList); },
                error: function (jqXHR) { render(jqXHR.responseJSON.message, []); }
            });
        });
        //--- click on the update button --------------------------------------------
        $('#updateBtn').on("click", function () {
            var vorname = $('#forenameInput').val().trim();
            var nachname = $('#surnameInput').val().trim();
            var nutzername = $('#nutzernameInput').val().trim();
            var passwort = $('#passwortInput').val().trim();
            var id = $('#userIDInput').val();
            var data = { "vorname": vorname, "nachname": nachname, "nutzername": nutzername, "passwort": passwort };
            $.ajax({
                url: 'http://localhost:8080/user/' + id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: 'application/json',
                dataType: 'json',
                success: function (data) { render(data.message, data.userList); },
                error: function (jqXHR) { render(jqXHR.responseJSON.message, []); }
            });
        });
        //--- click on the delete button --------------------------------------------
        $('#deleteBtn').on("click", function () {
            var id = $('#userIDInput').val();
            $.ajax({
                url: 'http://localhost:8080/user/' + id,
                type: 'DELETE',
                dataType: 'json',
                success: function (data) { render(data.message, data.userList); },
                error: function (jqXHR) { render(jqXHR.responseJSON.message, []); }
            });
        });
        //--- click on the read list button ----------------------------------------
        $('#readListBtn').on("click", function () {
            $.ajax({
                url: 'http://localhost:8080/users',
                type: 'GET',
                dataType: 'json',
                success: function (data) { render(data.message, data.userList); },
                error: function (jqXHR) { render(jqXHR.responseJSON.message, []); }
            });
        });
        //--- click on the delete list button --------------------------------------------
        $('#deleteListBtn').on("click", function () {
            $.ajax({
                url: 'http://localhost:8080/users',
                type: 'DELETE',
                dataType: 'json',
                success: function (data) { render(data.message, data.userList); },
                error: function (jqXHR) { render(jqXHR.responseJSON.message, []); }
            });
        });
    });
})(AJAX || (AJAX = {})); // end of namespace AJAX
//# sourceMappingURL=clientAJAX.js.map