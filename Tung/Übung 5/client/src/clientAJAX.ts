namespace AJAX {

/**********************************************************************************************************************
 * Class "User": represents all data of a user in the userList - class is exported  (needs ES6)                       *
 **********************************************************************************************************************/
class User {
	id       : number;
	vorname  : string;
	nachname : string;
	time     : string;
	nutzername: string;
	passwort: string;
}

/*****************************************************************************/
/* render function: takes a message and a userData-array                     */

/*****************************************************************************/

function render(message: string, userList: User[]): void {
	let buffer: string = "";
	//--- render message --------------------------------------------------------
	buffer += "<div id='serverMessage'> " + message + "</div>\n";
	//--- render table (only if userData exists) --------------------------------
	if (userList.length > 0) {
		buffer += "<div id='userList'>\n";
		buffer += "  <table>\n";
		buffer += "    <tr>\n";
		buffer += "      <th style='width:20px '> id       </th>\n";
		buffer += "      <th style='width:100px'> vorname  </th>\n";
		buffer += "      <th style='width:100px'> nachname </th>\n";
        buffer += "      <th style='width:100px'> nutzername </th>\n";
        buffer += "      <th style='width:100px'> passwort </th>\n";
		buffer += "    </tr>\n";
		for (let user of userList) { // iterate through array "userData"
			if (user != null) {  // ignore array-elements that have been deleted
				buffer += "    <tr id=user'" + user.id + "'>\n";
				buffer += "      <td> " + user.id + " </td>\n";
				buffer += "      <td> " + user.vorname + " </td>\n";
				buffer += "      <td> " + user.nachname + " </td>\n";
                buffer += "      <td> " + user.nutzername + " </td>\n";
                buffer += "      <td> " + user.passwort + " </td>\n";
				buffer += "    </tr>\n";
			}
		}
	}
	//--- close table (and div) ------------------------------------------------
	buffer += "  </table>\n";
	buffer += "</div>";
	//--- put buffer-string into message and userlist --------------------------
	$('#data').html(buffer);


	//
	// Examples of loops: All loops provide the same results
	//
	console.log("\n---------------------\n for-of Schleife)");
	for (let entry of userList) { // entry get value of all elements in list
		if (entry != null) {  // ignore array-elements that have been deleted
			console.log(entry.vorname + " " + entry.nachname);
		}
	}
	console.log("\n for-in Schleife");
	for (let j in userList) { // j get index of all elements in list
		if (userList[j] != null) {
			console.log(userList[j].vorname + " " + userList[j].nachname);
		}
	}
	console.log("\n for-Schleife)");
	for (let i = 0; i < userList.length; i++) { // index starts with 0 and end with length of list-1
		if (userList[i] != null) {
			console.log(userList[i].vorname + " " + userList[i].nachname);
		}
	}
	console.log("\n while-Schleife)");
	let i: number = 0;           // i start with 0
	while (i < userList.length) { // loop runs while i < length of list
		if (userList[i] != null) {
			console.log(userList[i].vorname + " " + userList[i].nachname);
		}
		i++; // increment i - i.e. i=i+1
	}


}


/*****************************************************************************/
/*  "Main" Callback funtion: triggered when page loaded                      */
/*****************************************************************************/
$(function () {

	//--- click on the create button --------------------------------------------
	$('#createBtn').on("click", () => {
		let vorname  : string = ($('#forenameInput').val() as string).trim();
		let nachname : string = ($('#surnameInput').val()  as string).trim();
        let nutzername : string = ($('#nutzernameInput').val()  as string).trim();
        let passwort : string = ($('#passwortInput').val()  as string).trim();
		let data     : Object = {"vorname": vorname, "nachname": nachname, "nutzername": nutzername, "passwort": passwort};
		$.ajax({                // set up ajax request
			url         : 'http://localhost:8080/user',
			type        : 'POST',    // POST-request for CREATE
			data        : JSON.stringify(data),
			contentType : 'application/json',  // using json in request
			dataType    : 'json',              // expecting json in response
			success     : (data)  => { render(data.message, data.userList)    },
			error       : (jqXHR) => { render(jqXHR.responseJSON.message, []) }
		});
	});

	//--- click on the read button ----------------------------------------------
	$('#readBtn').on("click", () => {
		let id: string = $('#userIDInput').val() as string;
		$.ajax({                // set up ajax request
			url      : 'http://localhost:8080/user/' + id,
			type     : 'GET',    // GET-request for READ
			dataType : 'json',   // expecting json
			success  : (data)  => { render(data.message, data.userList)    },
			error    : (jqXHR) => { render(jqXHR.responseJSON.message, []) }
		});
	});

	//--- click on the update button --------------------------------------------
	$('#updateBtn').on("click", () => {
		let vorname  : string = ($('#forenameInput').val() as string).trim();
		let nachname : string = ($('#surnameInput').val()  as string).trim();
        let nutzername : string = ($('#nutzernameInput').val() as string).trim();
        let passwort : string = ($('#passwortInput').val()  as string).trim();

		let id       : string =  $('#userIDInput').val()   as string;
		let data     : Object = {"vorname": vorname, "nachname": nachname, "nutzername": nutzername, "passwort": passwort};
		$.ajax({                // set up ajax request
			url         : 'http://localhost:8080/user/' + id,
			type        : 'PUT',    // PUT-request for UPDATE
			data        : JSON.stringify(data),
			contentType : 'application/json',  // using json in request
			dataType    : 'json',              // expecting json in response
			success     : (data) => { render(data.message, data.userList) },
			error       : (jqXHR) => { render(jqXHR.responseJSON.message, []) }
		});
	});

	//--- click on the delete button --------------------------------------------
	$('#deleteBtn').on("click", () => {
		let id: string = $('#userIDInput').val() as string;
		$.ajax({
			url      : 'http://localhost:8080/user/' + id,
			type     : 'DELETE',  // DELETE-request for DELETE
			dataType : 'json',    // expecting json
			success  : (data)  => { render(data.message, data.userList)    },
			error    : (jqXHR) => { render(jqXHR.responseJSON.message, []) }
		});
	});

	//--- click on the read list button ----------------------------------------
	$('#readListBtn').on("click", () => {
		$.ajax({
			url      : 'http://localhost:8080/users',
			type     : 'GET',     // GET-request for DELETE
			dataType : 'json',    // expecting json
			success  : (data)  => { render(data.message, data.userList)    },
			error    : (jqXHR) => { render(jqXHR.responseJSON.message, []) }
		});
	});

	//--- click on the delete list button --------------------------------------------
	$('#deleteListBtn').on("click", () => {
		$.ajax({
			url      : 'http://localhost:8080/users',
			type     : 'DELETE',  // DELETE-request for DELETE
			dataType : 'json',    // expecting json
			success  : (data) => { render(data.message, data.userList)   },
			error    : (jqXHR) => { render(jqXHR.responseJSON.message, []) }
		});
	});

});
} // end of namespace AJAX
