define({ "api": [
  {
    "type": "get",
    "url": "/raten/:zahl",
    "title": "Eine Zahl eraten",
    "group": "Frontend",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "target",
            "description": "<p>random (zahl)</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Request-Example:",
          "content": "{\n  \"zahl\": 5\n}",
          "type": "json"
        }
      ]
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "zahl",
            "description": "<p>zahl of target</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./express.ts",
    "groupTitle": "Frontend",
    "name": "GetRatenZahl"
  },
  {
    "type": "get",
    "url": "/cheat/:password",
    "title": "password abfragen",
    "group": "frontened",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>'password' == password</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./express.ts",
    "groupTitle": "frontened",
    "name": "GetCheatPassword"
  },
  {
    "type": "get",
    "url": "/reset/",
    "title": "function reset",
    "group": "frontened",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "message",
            "description": "<p>zahl</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "message",
            "description": "<p>'target' == random ()</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"reset\": \"okay\",\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./express.ts",
    "groupTitle": "frontened",
    "name": "GetReset"
  }
] });
