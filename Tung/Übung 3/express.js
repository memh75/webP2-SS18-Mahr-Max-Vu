"use strict";
exports.__esModule = true;
var express = require("express");
var bodyParser = require("body-parser");
var router = express();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.listen(8080);
console.log("Gestartet: http://localhost:8080/site/view.html");
router.use("/site", express.static(__dirname + "/Client"));
//# sourceMappingURL=express.js.map