"use strict";
exports.__esModule = true;
var express = require("express");
var bodyParser = require("body-parser");
var router = express();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.listen(8080);
console.log("Gestartet: http://localhost:8080/Client/zahl.html");
router.use("/site", express.static(__dirname + "/Client"));
function random() {
    var min = 1;
    var max = 10;
    return Math.floor(Math.random() * (max - min)) + min;
}
var target = random();
var password = "geheim";
router.get("/raten/:zahl", function (req, res) {
    var geraten = req.params["zahl"];
    if (isNaN(geraten)) {
        res.json({ antwort: "Fehler!" });
    }
    else if (geraten > target) {
        res.json({ antwort: "Kleiner..." });
    }
    else if (geraten < target) {
        res.json({ antwort: "Größer..." });
    }
    else {
        res.json({ antwort: "Richtig!" });
    }
});
router.get("/cheat/:password", function (req, res) {
    if (req.params["password"] == password) {
        res.json({ antwort: target });
    }
    else {
        res.json({ antwort: "Passwort abgelehnt!" });
    }
});
router.get("/reset", function (req, res) {
    target = random();
    res.json({ antwort: "okay" });
});
router.post("/set", function (req, res) {
    var newNumber = req.body["new"];
    var enteredPassword = req.body["password"];
    if (enteredPassword != password) {
        res.json({ antwort: "Passwort abgelehnt!" });
        return;
    }
    if (isNaN(newNumber) || newNumber == "") {
        res.json({ antwort: "Das ist keine Zahl!" });
        return;
    }
    target = newNumber;
    res.json({ antwort: "okay" });
});
//# sourceMappingURL=express.js.map