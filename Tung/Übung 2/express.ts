import {Request, Response} from "express";
import express = require("express");
import bodyParser = require("body-parser");

let router = express();
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({extended: true}));
router.listen(8080);

console.log("Gestartet: http://localhost:8080/Client/zahl.html");

router.use("/site",express.static(__dirname + "/Client"));

function random(): Number {
	let min = 1;
	let max = 10;
	return Math.floor(Math.random() * (max - min)) + min;
}

let target: Number = random();
let password: String = "geheim";

router.get("/raten/:zahl", function (req: Request, res: Response) {
	let geraten = req.params["zahl"];

	if (isNaN(geraten)) {
		res.json({antwort: "Fehler!"});
	}
	else if (geraten > target) {
    res.json({antwort: "Kleiner..."});
	}
	else if (geraten < target) {
    res.json({antwort: "Größer..."});
	}
	else {
    res.json({antwort: "Richtig!"});
	}
});

router.get("/cheat/:password", function (req: Request, res: Response) {
	if(req.params["password"] == password) {
		res.json({antwort: target});
	}
	else {
    res.json({antwort: "Passwort abgelehnt!"});
	}
});

router.get("/reset", function (req: Request, res: Response) {
	target = random();
  res.json({antwort: "okay"});
});

router.post("/set", function (req: Request, res: Response) {
	let newNumber = req.body["new"];
	let enteredPassword = req.body["password"];

	if(enteredPassword != password) {
    res.json({antwort: "Passwort abgelehnt!"});
    return;
	}

  if(isNaN(newNumber) || newNumber == "") {
    res.json({antwort: "Das ist keine Zahl!"});
    return;
  }


  target = newNumber;
	res.json({antwort: "okay"});
});
