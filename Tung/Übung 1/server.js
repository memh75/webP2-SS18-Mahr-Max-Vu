"use strict";
exports.__esModule = true;
var express = require("express");
var bodyParser = require("body-parser");
var router = express();
router.use(bodyParser.json());
router.listen(8080, function () {
    console.log("Gestartet!");
    console.log("Aufrufbar sollten sein: ");
    console.log("  http://localhost:8080/name");
    console.log("  http://localhost:8080/htmlfile");
    console.log("  http://localhost:8080/gauss");
    console.log("  http://localhost:8080/liste");
    console.log("  http://localhost:8080/object");
});
router.get("/name", function (req, res) {
    res.json({ vorname: "Tung", nachname: "Vu" });
});
router.use("/htmlfile", express.static(__dirname + '/name.html'));
router.get("/gauss", function (req, res) {
    var a = 0;
    for (var i = 1; i >= 100; i++) {
        a += i;
    }
    res.json({ ergebnis: a });
});
router.get("/liste", function (req, res) {
    var Zahlen = [];
    for (var i = 1; i <= 100; i++) {
        Zahlen.push(i);
    }
    res.json({ ergebnis: Zahlen });
});
var Person = /** @class */ (function () {
    function Person(_vorname, _nachname) {
        this.vorname = _vorname;
        this.nachname = _nachname;
    }
    return Person;
}());
router.get("/object", function (req, res) {
    var myObject = new Person("Tung", "Vu");
    res.json(myObject);
});
//# sourceMappingURL=server.js.map