"use strict";
exports.__esModule = true;
/*****************************************************************************
 ***  Import some module from node.js (see: expressjs.com/en/4x/api.html)    *
 *****************************************************************************/
var express = require("express"); // import EXPRESS
/*****************************************************************************
 ***  user class and array of users                                          *
 *****************************************************************************/
//--- class describing a user -------------------------------------------------
var User = /** @class */ (function () {
    // methods
    function User(id, vname, nname) {
        this.id = id;
        this.vorname = vname;
        this.nachname = nname;
        this.passwort = vname;
        this.time = new Date().toLocaleString('de-DE');
    }
    return User;
}());
//--- array of all users ------------------------------------------------------
var userList = [];
/*****************************************************************************
 ***  Create server with handler function and start it                       *
 *****************************************************************************/
var router = express();
router.listen(8080, "localhost", function () {
    console.log("\n    -------------------------------------------------------------\n    Client-Server usermanager\n    Dokumentation mit: im Terminal von webStorm\n    \"apidoc -o apidoc -e node_modules\n    starte lokal apidoc/index.html\n  \n\t  Aufruf (Zwei Client-Varianten):\n\t  - AJAX:      http://localhost:8080/clientAJAX.html\n\t  - BOOTSTRAP: http://localhost:8080/clientBS.html\n    -------------------------------------------------------------\n  ");
});
/*****************************************************************************
 ***  Static routers                                                         *
 *****************************************************************************/
var baseDir = __dirname + '/../..'; // get rid of /server/src
router.use("/", express.static(baseDir + "/client/views"));
router.use("/css", express.static(baseDir + "/client/css"));
router.use("/src", express.static(baseDir + "/client/src"));
router.use("/jquery", express.static(baseDir + "/client/node_modules/jquery/dist"));
router.use("/popper.js", express.static(baseDir + "/client/node_modules/popper.js/dist"));
router.use("/bootstrap", express.static(baseDir + "/client/node_modules/bootstrap/dist"));
router.use("/font-awesome", express.static(baseDir + "/client/node_modules/font-awesome"));
router.use(express.json()); // parsing json
/*****************************************************************************
 ***  Dynamic Routers                                                        *
 *****************************************************************************/
/**
 * --- common api-description: 400 BadRequest --------------------------
 * @apiDefine BadRequest
 * @apiError (Error 400) {string} message  description of the error
 * @apiError (Error 400) {json[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 400 (Bad Request) Parameter not provided
 * HTTP/1.1 400 Bad Request
 * {
 *   "message"  : "vorname or nachname not provided",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *   ]
 * }
 * @apiErrorExample 400 (Bad Request) wrong Parameter format
 * HTTP/1.1 400 Bad Request
 * {
 *   "message"  : "Id 'Hans' not a number",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *   ]
 * }
 * @apiErrorExample 400 (Bad Request) ID not provided
 * HTTP/1.1 400 Bad Request
 * {
 *   "message"  : "Parameter ID not provided",
 *   "userList" : [
 *     {"id":0, "vorname":"Max", "nachname":"Mustermann"},
 *   ]
 * }
 */
/**
 * --- common api-description: 404 NotFound ----------------------------
 * @apiDefine NotFound
 * @apiError (Error 404) {string} message  description of the error
 * @apiError (Error 404) {json[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 404 (Not Found) Example
 * HTTP/1.1 404 Not Found
 * {
 *   "message"  : "Id 42 out of index range",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *     {"vorname":"Sabine","nachname":"Musterfrau"}
 *   ]
 * }
 *
 */
/**
 * --- common api-description: 410 Gone --------------------------------
 * @apiDefine Gone
 * @apiError (Error 410) {string}   message  description of the error
 * @apiError (Error 410) {object[]} userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiErrorExample 410 (Gone) Example
 * HTTP/1.1 410 Gone
 * {
 *   "message"  : "User with id 2 already deleted",
 *   "userList" : [
 *     {"vorname":"Max",   "nachname":"Mustermann"},
 *     {"vorname":"Sabine","nachname":"Musterfrau"}
 *   ]
 * }
 */
/**
 * --- create new user with: post /user --------------------------------
 * @api        {post} /user Create new user
 * @apiVersion 1.0.0
 * @apiName    CreateUser
 * @apiGroup   User
 * @apiDescription
 * This route creates a new user with provided parameters and returns <br />
 * - a message with the attributes of the newly created user
 * - a userList containing all users
 * - a user-Object with all attributes of the created user<br/><br/>
 * @apiExample {url} Usage Example
 * http://localhost:8080/user
 *
 * @apiParam {string} vorname  surname of the user
 * @apiParam {string} nachname lastname of the user
 * @apiParamExample {json} Parameters Example
 * vorname=Max&nachname=Mustermann
 *
 * @apiSuccess (Success 201) {string}  message  attributes of newly created user
 * @apiSuccess (Success 201) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 201) {Object}  user that have been created: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 201 (Created) Example
 * HTTP/1.1 201 Created
 * { "message"  : "Sabine Musterfrau successfully added",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017 15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017 15:28:00"} ], }
 *
 * @apiUse BadRequest
 */
var cryptoJS = require("crypto-js"); // handles cryptographie
var db = require("mysql"); // handles database connections
var connection = db.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'userman'
});
function renderResult(message) {
    var query = 'SELECT id,time,username,vorname,nachname FROM userlist;';
    connection.query(query, function (err, rows) {
        if (!err) {
            console.log("\n" + message);
            for (var i = 0; i < rows.length; i++) {
                console.log("UserList :" +
                    rows[i].id + "; " +
                    rows[i].time + "; " +
                    rows[i].username + "; " +
                    rows[i].vorname + "; " +
                    rows[i].nachname);
            }
        }
        else {
            console.log("Select all error: " + err.code);
        }
    });
}
/*****************************************************************************
 ***  Connect database and CRUD user, one after the other                    *
 *****************************************************************************/
var query;
//---- connect to database ----------------------------------------------------
connection.connect(function (err) {
    if (!err) {
        console.log("Database is connected ...\n");
    }
    else {
        console.log("Error connecting database ...\n" + err);
    }
});
renderResult("Initial userList");
router.post("/user", function (req, res) {
    var vorname = (req.body.vorname ? req.body.vorname : "").trim();
    var nachname = (req.body.nachname ? req.body.nachname : "").trim();
    var message = "";
    //--- process parameters ----------------------------------------------------
    if ((vorname !== "") && (nachname !== "")) {
        var insertData_1 = [new Date().toLocaleString(), "MM", cryptoJS.MD5("secret1").toString(), "Max", "Mustermann"];
        // set up query - parameters are substituted by array-elements
        query = 'INSERT INTO userlist (time, username, password, vorname, nachname ) VALUES (?,?,?,?,?);';
        connection.query(query, insertData_1, function (err) {
            if (!err) {
                renderResult("Inserted " + insertData_1[1] + " (" + insertData_1[3] + " " + insertData_1[4] + ")");
            }
            else {
                renderResult("Insert error: " + err.code);
            }
        });
        message = vorname + " " + nachname + " successfully added";
        res.status(201); // set http status to "Created"
    }
    else {
        res.status(400); // set http status to "Bad Request"
        message = "vorname or nachname not provided";
    }
    //--- prepare and send response ---------------------------------------------
    res.json({ message: message, userList: userList });
});
/**
 * --- get user with /user/:id -----------------------------------------
 * @api        {get} /user/:id Read user information
 * @apiVersion 1.0.0
 * @apiName    ReadUser
 * @apiGroup   User
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be read
 *
 * @apiDescription
 * This route reads the attributes of a user with provided <code>id</code> and returns <br />
 * - a message with the attributes of user with id <code>id</code><br />
 * - an userList containing all users
 * - a user-Object with all attributes of the read user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/0
 *
 * @apiSuccess (Success 200) {string}  message  attributes of user with id <code>id</code>
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 200) {Object}  user that have been read: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Selected item is Max Mustermann",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Musterfrau", "time":"23.02.2017  15:28:00"} ],
 *   "user"     : { {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"} } }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
router.get("/user/:id", function (req, res) {
    var id = req.params.id;
    var message = "";
    var user;
    //--- process parameters ----------------------------------------------------
    var getData = ["MM"];
    query = 'SELECT * FROM userlist WHERE username = ?;';
    connection.query(query, getData, function (err, rows) {
        if (!err) {
            if (rows.length === 1) { // only one dataset must be found -> rows[0]
                renderResult("Read " + getData[0]);
            }
            else {
                renderResult("Select: Username not found");
            }
        }
        else {
            renderResult("Select error: " + err.code);
        }
    });
    //--- prepare and send response ---------------------------------------------
    res.json({ message: message, userList: userList, user: user });
});
/**
 * --- update user with: put /user/:id ---------------------------------
 * @api        {put} /user/:id Update user
 * @apiVersion 1.0.0
 * @apiName    UpdateUser
 * @apiGroup   User
 * @apiDescription
 * This route changes attributes of a user with provided <code>id</code><br />
 * Only the provided (optional) parameters are hanged.
 * "Update User" returns <br />
 * - a message with the attributes of the updated user
 * - a userList containing all users
 * - a user-Object with all attributes of the updated user<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/1
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be updated
 * @apiParam {string} [vorname]  surname of the users
 * @apiParam {string} [nachname] lastname of the user
 *
 * @apiParamExample {urlencoded} Parameters Example
 * vorname=Max&nachname=Mustermann
 *
 * @apiSuccess (Success 200) {string}  message  attributes of newly created user
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string, ...}, ...]
 * @apiSuccess (Success 200) {Object}  user the user-data after update: {"vorname":string, "nachname":string, ...}
 * @apiSuccessExample {json} 201 (Created) Example
 * HTTP/1.1 200 Ok
 * { "message"  : "Updated item is Sabine Mustermann",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann", "time":"23.02.2017  15:27:00"},
 *                  {"vorname":"Sabine","nachname":"Mustermann", "time":"23.02.2017  15:28:00"} ],
 *   "user"     : { {"vorname":"Sabine","nachname":"Mustermann", "time":"23.02.2017  15:28:00"} } }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
router.put("/user/:id", function (req, res) {
    var vorname = (req.body.vorname ? req.body.vorname : "").trim();
    var nachname = (req.body.nachname ? req.body.nachname : "").trim();
    var id = req.params.id;
    var message = "";
    //--- process parameters ----------------------------------------------------
    var updateData = [cryptoJS.MD5("PasswortNeu").toString(), "Maximilian", "Mustermann-Musterfrau", "MM"];
    query = 'UPDATE userlist SET password = ?, vorname = ?, nachname = ? WHERE username = ?;';
    connection.query(query, updateData, function (err, rows) {
        if (!err) {
            if (rows.affectedRows === 1) { // only one dataset must be affected
                renderResult("Updated " + updateData[3] + " with " + updateData[1] + " " + updateData[2]);
            }
            else {
                renderResult("Update: Username not found");
            }
        }
        else {
            renderResult("Update error: " + err.code);
        }
    });
    //--- prepare and send response ---------------------------------------------
    res.json({ message: message, userList: userList });
});
/**
 * --- delete user with /user/:id --------------------------------------
 * @api        {delete} /user/:id Delete user
 * @apiVersion 1.0.0
 * @apiName    DeleteUser
 * @apiGroup   User
 *
 * @apiDescription
 * This route deletes a user with provided <code>id</code> and returns <br />
 * - a message with the attributes of user with id <code>id</code><br />
 * - a userList containing all users<br/><br/>
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user/0
 *
 * @apiParam {number} :id  URL-parameter: <code>id</code> of the user to be deleted
 *
 * @apiSuccess (Success 200) {string}  message  attributes of user with id <code>id</code>
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiSuccessExample {json} 200 (ok) Example
 * HTTP/1.1 200 ok
 * { "message"  : "Max Mustermann has been deleted",
 *   "userList" : [ {"vorname":"Sabine","nachname":"Musterfrau"} ] }
 *
 * @apiUse BadRequest
 * @apiUse NotFound
 * @apiUse Gone
 */
router["delete"]("/user/:id", function (req, res) {
    var id = req.params.id;
    var message = "";
    //--- process parameters ----------------------------------------------------
    var deleteData = ["MM"];
    query = 'DELETE FROM userlist WHERE username = ?;';
    connection.query(query, deleteData, function (err, rows) {
        if (!err) {
            if (rows.affectedRows > 0) {
                renderResult("Deleted " + deleteData[0]);
            }
            else {
                renderResult("Delete: Username not found");
            }
        }
        else {
            renderResult("Delete error: " + err.code);
        }
    });
    console.log("\n\nProgram has been started ...\n\n");
    //--- prepare and send response ---------------------------------------------
    res.json({ message: message, userList: userList });
});
/**
 * --- corrupted REST-API command: all /user (ID missing) ---------------------
 * @api        {all} /user corrupted: ID missing
 * @apiVersion 1.0.0
 * @apiName    WrongAPIcall
 * @apiGroup   User
 * @apiDescription
 * This routes checks for API-call with no ID provided
 *
 * @apiExample {url} Usage Example
 * http://localhost:8080/user
 *
 * @apiUse BadRequest
 */
router.all("/user", function (req, res) {
    res.status(400);
    res.json({ message: "Parameter ID not provided", "userList": userList });
});
/**
 * --- read user list with: get /users ---------------------------------
 * @api        {get} /users Read list of users
 * @apiVersion 1.0.0
 * @apiName    readUsers
 * @apiGroup   Users
 * @apiDescription
 * This route returns <br />
 * - a message with the numbers of users in list
 * - a userList containing all users - possibly empty<br/><br/>
 * @apiExample {url} Usage Example
 * http://localhost:8080/users
 *
 * @apiSuccess (Success 200) {string}  message  numbers of users in list
 * @apiSuccess (Success 200) {json[]}  userList List of users: [{"vorname":string, "nachname":string}, ...]
 * @apiSuccessExample {json} 200 (Ok) Example
 * HTTP/1.1 200 Ok
 * { "message"  : "There are 2 users in list",
 *   "userList" : [ {"vorname":"Max",   "nachname":"Mustermann"},
 *                  {"vorname":"Sabine","nachname":"Musterfrau"} ] }
 */
router.get("/users", function (req, res) {
    var message;
    var id = userList.length;
    var noElements = 0;
    //--- construct message ----------------------------------------------------
    var getData = ["MM"];
    query = 'SELECT * FROM userlist;';
    connection.query(query, getData, function (err, rows) {
        if (!err) {
            if (rows.length === 1) { // only one dataset must be found -> rows[0]
                renderResult("Read " + getData[0]);
            }
            else {
                renderResult("Select: Username not found");
            }
        }
        else {
            renderResult("Select error: " + err.code);
        }
    });
    res.json({ message: message, userList: userList }); // return message
});
/**
 * --- delete user list with: delete /users ----------------------------
 * @api        {delete} /users Delete list of users
 * @apiVersion 1.0.0
 * @apiName    DeleteUsers
 * @apiGroup   Users
 * @apiDescription
 * This route deletes the list of users and return <br />
 * - a message with the numbers of users in list that have been deleted
 * - a userList containing all users - possibly empty<br/><br/>
 * @apiExample {url} Usage Example
 * http://localhost:8080/users
 *
 * @apiSuccess (Success 200) {string}  message  numbers of users in list that have been deleted
 * @apiSuccess (Success 200) {json[]}  userList empty list of users: []
 * @apiSuccessExample {json} 200 (Ok) Example
 * HTTP/1.1 200 Ok
 * { "message"  : "2 users have been deleted",
 *   "userList" : []                            }
 */
router["delete"]("/users", function (req, res) {
    var message;
    var id = userList.length;
    var noElements = 0;
    //--- construct message ----------------------------------------------------
    var deleteData = ["MM"];
    query = 'DELETE * FROM userlist ;';
    connection.query(query, deleteData, function (err, rows) {
        if (!err) {
            if (rows.affectedRows > 0) {
                renderResult("Deleted " + deleteData[0]);
            }
            else {
                renderResult("Delete: Username not found");
            }
        }
        else {
            renderResult("Delete error: " + err.code);
        }
    });
    console.log("\n\nProgram has been started ...\n\n");
    //--- prepare and send response ---------------------------------------------
    res.json({ message: message, userList: userList }); // return message
});
//# sourceMappingURL=serverCS.js.map