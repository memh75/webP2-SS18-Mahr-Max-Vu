
function getSession() {

    $.ajax({                // set up ajax request
        url: 'http://localhost:8080/session',
        type: 'GET',    // GET-request for READ
        dataType: 'json',   // expecting json
        success: (data) => {
            console.log("getSession", data);
            $('#SessionCode').find('code').html(JSON.stringify(data));
        },
        error: (jqXHR) => {
            $('#demo').attr({msg: jqXHR.responseJSON.message});
            $('#demo').click();
        }
    });

}

function deleteSession() {

    $.ajax({                // set up ajax request
        url: 'http://localhost:8080/session',
        type: 'DELETE',    // GET-request for READ
        dataType: 'json',   // expecting json
        success: (data) => {
            console.log("deleteSession", data);
            getSession();
            $('#demo').attr({msg: "Session gelöscht!"});
            $('#demo').click();
        },
        error: (jqXHR) => {
            $('#demo').attr({msg: jqXHR.responseJSON.message});
            $('#demo').click();
        }
    });

}

function Variablesetzen() {

    let key = $('#deleteVariableKey');
    let val = $('#Variableanlegen');


    if (key.val() != "" && val.val() != "") {
        $.ajax({                // set up ajax request
            url: 'http://localhost:8080/variable/' + key.val() + '/' + val.val(),
            type: 'POST',    // GET-request for READ
            dataType: 'json',   // expecting json
            success: (data) => {
                console.log("setVariable", data);
                getSession();
                key.val("").parent().removeClass('is-dirty');
                val.val("").parent().removeClass('is-dirty');
                $('#demo').attr({msg: "Session-Variable gesetzt!"});
                $('#demo').click();
            },
            error: (jqXHR) => {
                $('#demo').attr({msg: jqXHR.responseJSON.message});
                $('#demo').click();
            }
        });
    }
}

function Variablelöschen() {

    let key = $('#deleteVariableKey');

    if (key.val() != "") {
        $.ajax({                // set up ajax request
            url: 'http://localhost:8080/variable/' + key.val(),
            type: 'DELETE',    // GET-request for READ
            dataType: 'json',   // expecting json
            success: (data) => {
                console.log("deleteVariable", data);
                getSession();
                key.val("").parent().removeClass('is-dirty');
                $('#demo').attr({msg: "Session-Variable gelöscht!"});
                $('#demo').click();
            },
            error: (jqXHR) => {
                $('#demo').attr({msg: jqXHR.responseJSON.message});
                $('#demo').click();
            }
        });
    }

}



$(document).ready(function() {

    getSession();

    $("#getSessionBtn").on("click", () => {
        getSession();
    });

    $("#deleteSessionBtn").on("click", () => {
        deleteSession();
    });

    $("#VariableBtn").on("click", () => {
        Variablesetzen();
    });

    $("#deleteVariableBtn").on("click", () => {
        Variablelöschen();
    });


});
